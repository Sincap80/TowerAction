﻿using UnityEngine;
using UnityEngine.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class LivesUI : NetworkBehaviour{

    public Text livesText;
    public GameObject gameManager;
	void Update () {
       livesText.text = gameManager.GetComponent<TeamStats>().Lives + " Lives";
	}

}
