﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class ShootBullet : NetworkBehaviour
{
    //Drag in the Bullet Emitter from the Component Inspector.
    public GameObject bulletSpawn;

    //Drag in the Bullet Prefab from the Component Inspector.
    public GameObject bulletPrefab;

    public Camera cam;

    //Enter the Speed of the Bullet from the Component Inspector.
    public float Bullet_Forward_Force;



    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Ray ray = cam.ViewportPointToRay(new Vector3(0.5F, 0.2F, 0));

            CmdFire(ray.direction);

        }
        
    }

    [Command]
    void CmdFire(Vector3 direction)
    {
        // Create the Bullet from the Bullet Prefab
        GameObject bullet = (GameObject)Instantiate(
            bulletPrefab,
            bulletSpawn.transform.position,
            bulletSpawn.transform.rotation);

        // Add velocity to the bullet rigidbody.AddForce(direction.forward * strength);
        

        // Spawn the bullet on the Clients
        NetworkServer.Spawn(bullet);

        Debug.Log(direction);
        bullet.GetComponent<Rigidbody>().AddForce(this.transform.forward.normalized * Bullet_Forward_Force);

        // Destroy the bullet after 2 seconds
        Destroy(bullet, 5.0f);
    }



}