using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Enemy : NetworkBehaviour {

    [SyncVar]
    public float actualSpeed;

    public float startSpeed = 6f;

    [SyncVar(hook = "OnChangeHealth")]
    public float health;

    public float startHealth = 100;
    public int moneyGain = 50;

    public GameObject deathEffect;
    
    //For PlayerStats Variables
    public GameObject gameMaster;

    [Header("Unity Stuff")]
    public Canvas healthCanvas;
    public Image healthBar;

    
    void Start()
    {
        actualSpeed = startSpeed;
        health = startHealth;
    }

    void Update()
    {
       // healthCanvas.transform.LookAt(GameObject.FindGameObjectWithTag("MainCamera").transform);
    }

    public void TakeDamage (float amount)
    {
        if (!isServer)
        {
            return;
        }

        health -= amount;
        
        if (health <= 0)
        {
            CmdDie();
        }
    }


    void OnChangeHealth(float health)
    {
        healthBar.fillAmount = health / startHealth;
    }

    public void Slow(float slowingPct)
    {
        actualSpeed = startSpeed * (1f - slowingPct);
    }

    [Command]
    void CmdDie ()
    {
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<TeamStats>().Money += moneyGain;
        GameObject effect = (GameObject) Instantiate(deathEffect, transform.position, Quaternion.identity);
        NetworkServer.Spawn(effect);

        Destroy(effect, 5f);

        GameObject.FindGameObjectWithTag("GameMaster").GetComponent<WaveSpawner>().EnemiesAlive--;
        NetworkServer.Destroy(gameObject);
        Destroy(gameObject);
    }

}
