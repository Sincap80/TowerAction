﻿using UnityEngine;
using UnityEngine.Networking;

public class BuildManager : NetworkBehaviour {

    public static BuildManager instance;
    public Node targetNode;
    public GameObject gameManager;
    void Awake()
    {
        if(instance != null)
        {
            Debug.LogError("More than one BuildManage in scene");
            return;
        }
        instance = this;
    }

    private TurretBlueprint turretToBuild;
    private Node selectedNode;

    public GameObject buildEffect;
    public NodeUI nodeUI;
    public bool CanBuild { get { return turretToBuild != null; } }
    public bool HasMoney { get { return gameManager.GetComponent<TeamStats>().Money >= turretToBuild.cost; } }

    public void SelectTurretToBuild(TurretBlueprint turret)
    {
        turretToBuild = turret;
        DeselectNode();
    }

    public void SelectNode(Node node)
    {
        if(selectedNode == node)
        {
            DeselectNode();
            return;
        }
        selectedNode = node;
        turretToBuild = null;
        
        nodeUI.SetTarget(node);
    }

    public void DeselectNode()
    {
        selectedNode = null;
        nodeUI.Hide();
    }

    public TurretBlueprint GetTurretToBuild()
    {
        return turretToBuild;
    }

	
}
