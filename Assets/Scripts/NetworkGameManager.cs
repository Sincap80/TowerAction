﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Prototype.NetworkLobby;
using System.Collections;
using System.Collections.Generic;

public class NetworkGameManager : NetworkBehaviour
{
    public Image ClientLivesUI;
    
    [SyncVar(hook = "OnChangegameisOver")]
    public bool displayGameOverUI;

    public bool gameOver = false;

    public GameObject gameOverUI;
    public GameObject Player;
    public GameObject go;
    public GameObject[] playerInformationGo;

    void Start()
    {
        playerInformationGo = GameObject.FindGameObjectsWithTag("PlayerInformation");

        Debug.Log(playerInformationGo[0].GetComponent<PlayerInformation>().playerType);
        
        
        if (isServer)
        {
            
            ClientLivesUI.enabled = false;
        }
       
        
    }
    [ServerCallback]
    void Update()
    {        
        if (GetComponent<TeamStats>().Lives <= 0)
            displayGameOverUI = true;
        
        if (!gameOver)
            return;
                
        StartCoroutine(ReturnToLoby());
    }

    public void ReturnToLobbyButton()
    {
        gameOver = true;
    }
    IEnumerator ReturnToLoby()
    {
        gameOver = false;
        yield return new WaitForSeconds(2.0f);
        LobbyManager.s_Singleton.ServerReturnToLobby();
    }

    public void OnChangegameisOver(bool isGameOver)
    {
        gameOverUI.SetActive(true);
    }
    
    
}
