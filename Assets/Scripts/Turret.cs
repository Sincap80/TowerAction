using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Turret : NetworkBehaviour
{
	private Transform target;
    private Enemy targetEnemy;

	[Header("General")]
	public float range = 15f;

    [Header("Use Bullets (default)")]
    public float fireRate = 1f;
    //[SyncVar]
	private float fireCountdown = 0f;

    [Header("Use Laser")]
    public bool useLaser = false;
    public int damageOverTime = 30;
    public ParticleSystem impactEffect;
    public Light impactLight;
    public float slowingAmount = 0.5f;
    [SyncVar(hook = "OnBeamActive")]
    public bool isBeam = false;
    private LineRenderer Beam;

    [Header("Unity Setup Fields")]
	public GameObject bulletPrefab;
    public Transform partToRotate;
    public Transform firePoint;
    public string enemyTag = "Enemy";
    public float turnSpeed = 10f;


    void Start () {
        Beam = GetComponent<LineRenderer>();
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
	}


	void UpdateTarget ()
	{
        GameObject[] enemies = null;

        if (enemyTag != null) { 
		    enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        }
        if (enemies != null) { 
            float shortestDistance = Mathf.Infinity;
		    GameObject nearestEnemy = null;
		    foreach (GameObject enemy in enemies)
		    {
			    float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
			    if (distanceToEnemy < shortestDistance)
			    {
				    shortestDistance = distanceToEnemy;
				    nearestEnemy = enemy;
			    }
		    }

		    if (nearestEnemy != null && shortestDistance <= range)
		    {
			    target = nearestEnemy.transform;
                targetEnemy = nearestEnemy.GetComponent<Enemy>();

            } else
		    {
			    target = null;
		    }
        }
    }
    
	// Update is called once per frame
	void Update () {
        if (target == null)
        {
            if (useLaser)
            {
                Beam.enabled = false;
                isBeam = false;
                impactEffect.Stop();
                impactLight.enabled = false;
            }
            return;
        }

        //Target lock on
        LockOnTarget();

        if (useLaser)
        {
            CmdLaser();
        }else
        {
            if (fireCountdown <= 0f)
            {
                 CmdShoot();
                fireCountdown = 1f / fireRate;
            }

            fireCountdown -= Time.deltaTime;
        }
		

	}
    void LockOnTarget()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);

    }
    void CmdLaser()
    {
        targetEnemy.TakeDamage(damageOverTime * Time.deltaTime);
        targetEnemy.Slow(slowingAmount);

        isBeam = false;
        isBeam = true;
        
        Vector3 dir = firePoint.position - target.position;
        impactEffect.transform.position = target.position + dir.normalized;
        impactEffect.transform.rotation = Quaternion.LookRotation(dir);
    }

    public void OnBeamActive(bool isOn)
    {
        if (target != null)
        {
            Beam = GetComponent<LineRenderer>(); 
            if (!isOn)
            { 
                return;
            }
            isBeam = isOn;
            Beam.enabled = true;
            Beam.SetPosition(0, firePoint.position);
            Beam.SetPosition(1, target.position);
        }
    }
    
    void CmdShoot()
    {
               
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();
        bullet.seekTarget = target;
    }


	void OnDrawGizmosSelected ()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, range);
	}
}
