﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Node : NetworkBehaviour
{
    [HideInInspector]
    public GameObject turret;
    [HideInInspector]
    public TurretBlueprint TurretBlueprint;
    [HideInInspector]
    public bool isUpgraded = false;


    public GameObject gameManager;
    public Vector3 positionOffset;

    BuildManager buildManager;
    TurretBlueprint blueprint;

    private Animator ShopBGAnim;

    void Start()
    {
        buildManager = BuildManager.instance;
        ShopBGAnim = GameObject.Find("UI/OverlayCanvas/ShopBG").GetComponent<Animator>();
        ShopBGAnim.enabled = false;
    }
    
    public Vector3 GetBuildPosition()
    {
        return transform.position + positionOffset;
    }
    void OnMouseDown()
    {
        
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        

        if (turret != null)
        {
            buildManager.SelectNode(this);
            return;
        }
        
        if (!buildManager.CanBuild)
            return;

        
        blueprint = buildManager.GetTurretToBuild();
        CmdBuildTurret();
        buildManager.SelectTurretToBuild(null);
    }

    public void SellTurret()
    {
        gameManager.GetComponent<TeamStats>().Money += TurretBlueprint.GetSellAmount();
        Destroy(turret);
        NetworkServer.Destroy(turret);
        TurretBlueprint = null;
    }

    [Command]
    void CmdBuildTurret ()
    {

        if (gameManager.GetComponent<TeamStats>().Money < blueprint.cost)
        {
            Debug.Log("Not enough Money");
            ShopBGAnim.enabled = true;
            return;
        }
        Debug.Log(blueprint.cost);
        gameManager.GetComponent<TeamStats>().Money -= blueprint.cost;
        
        GameObject _turret = (GameObject)Instantiate(blueprint.prefab, GetBuildPosition(), Quaternion.identity);
        turret = _turret;

        TurretBlueprint = blueprint;
        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);

        NetworkServer.Spawn(turret);
        NetworkServer.Spawn(effect);

        Debug.Log("Turret Build!");
    }

    [Command]
    public void CmdUpgradeTurret()
    {
        if (gameManager.GetComponent<TeamStats>().Money < TurretBlueprint.upgradeCost)
        {
            Debug.Log("Not enough Money to Upgrade");
            ShopBGAnim.enabled = true;
            return;
        }

        gameManager.GetComponent<TeamStats>().Money -= TurretBlueprint.upgradeCost;

        //Destroy the old turret
        Destroy(turret);
        NetworkServer.Destroy(turret);

        //Build the upgraded turret
        GameObject _turret = (GameObject)Instantiate(blueprint.upgradedPrefab, GetBuildPosition(), Quaternion.identity);
        turret = _turret;

        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);

        NetworkServer.Spawn(turret);
        NetworkServer.Spawn(effect);

        isUpgraded = true;
        Debug.Log("Turret Upgraded!");
    }
}
