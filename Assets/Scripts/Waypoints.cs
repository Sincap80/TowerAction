using UnityEngine;

public class Waypoints : MonoBehaviour
{

    public static Transform[] points;
    public Transform[] ways;

    void Awake()
    { 
        setWaypoints(0);
    }

    public void setWaypoints(int way)
    {
        points = new Transform[ways[way].childCount];
        for (int i = 0; i < points.Length; i++)
        {
            points[i] = ways[way].GetChild(i);
        }
    }
}
