﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Prototype.NetworkLobby;
using System.Collections;

public class GameOver : NetworkBehaviour {
    
    public GameObject ui;
    private bool gameOver = false;
    

    public void Menu()
    {
        // NetworkManager.singleton.StopClient();
        // NetworkManager.singleton.StopHost();

        // NetworkLobbyManager.singleton.StopClient();
        // NetworkLobbyManager.singleton.StopServer();

        //NetworkServer.DisconnectAll();
        //Network.Disconnect ();

        gameOver = true;
    }

    [ServerCallback]
    void Update()
    {
        if(gameOver)
            StartCoroutine(ExitDelay());
    }
    
    IEnumerator ExitDelay()
    {
        //yield return new WaitForSeconds(0.1f);
        //Destroy(NetworkLobbyManager.singleton.gameObject);

        yield return new WaitForSeconds(3f);
        //sceneManager.LoadScene(0);

        LobbyManager.s_Singleton.SendReturnToLobby();
        LobbyManager.s_Singleton.ServerReturnToLobby();
        
    }
}
