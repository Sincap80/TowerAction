﻿using UnityEngine;
using UnityEngine.Networking;

public class GameManager : NetworkBehaviour {

    [SyncVar(hook = "OnChangegameisOver")]
    public bool gameisOver = false;
    public GameObject gameOverUI;
    public GameObject Player;
    void Start()
    {
        Instantiate(Player);
        if (isServer)
        {
            GameObject Camera = GameObject.Find("unitychan(Clone)/CamPos/Camera");
            Camera.GetComponent<Camera>().enabled = false;
            Camera.GetComponent<AudioListener>().enabled = false;
        } else
        {
            GameObject.Find("MainCamera").GetComponent<Camera>().enabled = false;
            GameObject.Find("MainCamera").GetComponent<AudioListener>().enabled = false;
        }
    }
    
    void Update () {
        
        if (gameisOver)
            return;
        /*
        if (GetComponent<TeamStats>().Lives <= 0)
        {
            gameisOver = true;
        }*/
    }

    public void OnChangegameisOver(bool isGameOver)
    {
        gameOverUI.SetActive(true);
        Time.timeScale = 0f;
    }
}