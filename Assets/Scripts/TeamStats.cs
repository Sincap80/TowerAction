﻿using UnityEngine;
using UnityEngine.Collections;
using UnityEngine.Networking;

public class TeamStats : NetworkBehaviour {

    public int startLives = 20;
    public int startMoney = 400;

    [SyncVar]
    public int Money;

    [SyncVar]
    public int Lives;

    [SyncVar]
    public int Rounds;

    void Start()
    {
        Money = startMoney;
        Lives = startLives;
        Rounds = 0;
    }
}
