using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Bullet : NetworkBehaviour
{
    public Transform seekTarget;
    public Transform target;
    private Transform enemy;

    public float speed = 70f;
    public float explosionRadius = 0f;
    public GameObject impactEffect;
    public int damage = 50;
        
    void Update()
    {
        target = seekTarget;
        if (target == null)
        {
            Destroy(gameObject);
            NetworkServer.Destroy(gameObject);
            return;
        }

        
        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            CmdHitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);

    }
    
    void CmdHitTarget()
    {
        GameObject effectIns = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effectIns, 5f);

        if (explosionRadius > 0f)
        {
            Explode();
        }
        else
        {
            enemy = target;
            CmdDamage();
        }

        Destroy(gameObject);
    }


    void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy")
            {
                enemy = collider.transform;
                CmdDamage();
            }
        }
    }
    
    void CmdDamage()
    {
        Enemy e = enemy.GetComponent<Enemy>();

        if(e != null)
        {
            e.TakeDamage(damage);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}