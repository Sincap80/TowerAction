﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NodeUI : MonoBehaviour {

    public GameObject ui;
    public Text upgradeCost;
    public Text sellAmount;

    public Button upgradeButton;

    private Node target;
    

    public void SetTarget(Node _target)
    {
        target = _target;
        
        transform.position = target.GetBuildPosition();

        if (!target.isUpgraded)
        {
            upgradeCost.text = target.TurretBlueprint.upgradeCost + "€";
            upgradeButton.interactable = true;
        } else
        {
            upgradeCost.text = "Upgraded!";
            upgradeButton.interactable = false;
        }

        sellAmount.text = target.TurretBlueprint.GetSellAmount() + "€";
        ui.SetActive(true);
    }

    public void Hide()
    {
        ui.SetActive(false);
    }

    public void Upgrade()
    {
        target.CmdUpgradeTurret();
        BuildManager.instance.DeselectNode();
    }

    public void Sell()
    {
        target.SellTurret();
        BuildManager.instance.DeselectNode();
    }
}
