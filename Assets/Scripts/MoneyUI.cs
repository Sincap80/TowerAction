﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class MoneyUI : MonoBehaviour
{
    public Text moneyText;
    public GameObject gameManager;

    void Update()
    {
        moneyText.text = gameManager.GetComponent<TeamStats>().Money + "€";
    }
}