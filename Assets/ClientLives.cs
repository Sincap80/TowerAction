﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClientLives : MonoBehaviour {

    public Text livesText;
    public GameObject gameManager;
    void Update()
    {
        livesText.text = gameManager.GetComponent<TeamStats>().Lives + " Lives";
    }

}
