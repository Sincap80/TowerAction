﻿//
// Mecanimのアニメーションデータが、原点で移動しない場合の Rigidbody付きコントローラ
// サンプル
// 2014/03/13 N.Kobyasahi
//
using UnityEngine;
using System.Collections;

// 必要なコンポーネントの列記
[RequireComponent(typeof (Animator))]
[RequireComponent(typeof (CapsuleCollider))]
[RequireComponent(typeof (Rigidbody))]

public class UnityChanControlScriptWithRgidBody : MonoBehaviour
{

	public float animSpeed = 1.5f;
	public float lookSmoother = 3.0f;
	public bool useCurves = true;
												
	public float useCurvesHeight = 0.5f;	
    
	public float forwardSpeed = 7.0f;
	public float backwardSpeed = 2.0f;
    public float strafeSpeed = 5.0f;
	
	public float rotateSpeed = 100.0f;
	
	public float jumpPower = 3.0f; 
	
	private CapsuleCollider col;
	private Rigidbody rb;
	
	private Vector3 velocity_z;
    private Vector3 velocity_x;
	
	private float orgColHight;
	private Vector3 orgVectColCenter;
	
	private Animator anim;
	private AnimatorStateInfo currentBaseState;

	private GameObject cameraObject;
	
	static int idleState = Animator.StringToHash("Base Layer.Idle");
	static int locoState = Animator.StringToHash("Base Layer.Locomotion");
	static int jumpState = Animator.StringToHash("Base Layer.Jump");
	static int restState = Animator.StringToHash("Base Layer.Rest");
    
	void Start ()
	{
		anim = GetComponent<Animator>();
		col = GetComponent<CapsuleCollider>();
		rb = GetComponent<Rigidbody>();
		cameraObject = GameObject.FindWithTag("PlayerModelCamera");
		orgColHight = col.height;
		orgVectColCenter = col.center;
    }
	
	
	void FixedUpdate ()
	{
        //Keyboad
        float h = Input.GetAxis("Mouse X");
        float v = Input.GetAxis("Mouse Y");
        
        anim.speed = animSpeed;
        currentBaseState = anim.GetCurrentAnimatorStateInfo(0);
        rb.useGravity = true;
        anim.SetFloat("Speed", 0f);
        anim.SetFloat("Direction", 0f);
        
        // Strafe left right
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            velocity_x = new Vector3(1, 0, 0);
            if (Input.GetKey(KeyCode.D))
            {
                velocity_x *= strafeSpeed;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                velocity_x *= -1f*strafeSpeed;
            }
            
            velocity_x = transform.TransformDirection(velocity_x);
            transform.localPosition += velocity_x * Time.fixedDeltaTime;
        }
        // Forward Backwadrd

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
        {
            velocity_z = new Vector3(0, 0, 1);
            if (Input.GetKey(KeyCode.W))
            {
                velocity_z *= forwardSpeed;
            }
            else if (Input.GetKey(KeyCode.S))
            {
                velocity_z *= -1f*backwardSpeed;
            }
            anim.SetFloat("Speed", 1);
            
            velocity_z = transform.TransformDirection(velocity_z);
            transform.localPosition += velocity_z * Time.fixedDeltaTime;
            
        }

        if (h < 0.1f || h > 0.1f)
        { 
            //TODO: hier übere mehrere frames ruhen lassen .. 
            anim.SetFloat("Direction", h);
            transform.Rotate(0, h * rotateSpeed, 0);
        }







        if (Input.GetButtonDown("Jump"))
        {

            if (currentBaseState.nameHash == locoState)
            {
                if (!anim.IsInTransition(0))
                {
                    rb.AddForce(Vector3.up * jumpPower, ForceMode.VelocityChange);
                    anim.SetBool("Jump", true);
                }
            }
        }
        if (currentBaseState.nameHash == locoState)
        {
			if(useCurves)
            {
				resetCollider();
			}
		}
		else if(currentBaseState.nameHash == jumpState)
		{
			cameraObject.SendMessage("setCameraPositionJumpView");
			if(!anim.IsInTransition(0))
			{
				if(useCurves){
					float jumpHeight = anim.GetFloat("JumpHeight");
					float gravityControl = anim.GetFloat("GravityControl"); 
					if(gravityControl > 0)
						rb.useGravity = false;
										
					Ray ray = new Ray(transform.position + Vector3.up, -Vector3.up);
					RaycastHit hitInfo = new RaycastHit();
					if (Physics.Raycast(ray, out hitInfo))
					{
						if (hitInfo.distance > useCurvesHeight)
						{
							col.height = orgColHight - jumpHeight;		
							float adjCenterY = orgVectColCenter.y + jumpHeight;
							col.center = new Vector3(0, adjCenterY, 0);	
						}
						else{				
							resetCollider();
						}
					}
				}		
				anim.SetBool("Jump", false);
			}
		}
		else if (currentBaseState.nameHash == idleState)
		{
			if(useCurves){
				resetCollider();
			}
			if (Input.GetButtonDown("Jump")) {
				anim.SetBool("Rest", true);
			}
		}
		else if (currentBaseState.nameHash == restState)
		{
			if(!anim.IsInTransition(0))
			{
				anim.SetBool("Rest", false);
			}
		}
	}

	
	void resetCollider()
	{
		col.height = orgColHight;
		col.center = orgVectColCenter;
	}
}
