﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerInformation : NetworkBehaviour {


    [SyncVar]
    public string playerName = "";
    [SyncVar]
    public Color playerColor = Color.white;
    [SyncVar]
    public int playerType = 1;
    
    public GameObject playerPrefab;

    private GameObject[] playerInformationGo;

    private GameObject spawnPoint;

    // Use this for initialization
    void Start () {
        spawnPoint = GameObject.Find("SpawnPoint");
        playerInformationGo = GameObject.FindGameObjectsWithTag("PlayerInformation");

        //Debug.Log(playerInformationGo[0].GetComponent<PlayerInformation>().playerType);


        

    }
	
	// Update is called once per frame
	void Update () {

        if (isServer)
        {
            Debug.Log("Server goin");
            GameObject Camera = GameObject.Find("SpawnPoint/unitychan(Clone)/CamPos/Camera");
            Camera.GetComponent<Camera>().enabled = false;
            Camera.GetComponent<AudioListener>().enabled = false;
        }

        foreach (GameObject g in playerInformationGo)
        {
            if (g.GetComponent<PlayerInformation>().playerType == 2 && g.GetComponent<PlayerInformation>().hasAuthority)
            {
                GameObject Camera = GameObject.Find("SpawnPoint/unitychan(Clone)/CamPos/Camera");
                if (Camera == null) {

                    Debug.Log("Shooter" + g);
                    GameObject.Find("MainCamera").SetActive(false);
                    CmdSpawnPlayer();
                }
            }
        }
    }
    


    [Command]
    public void CmdSpawnPlayer()
    {
        Debug.Log("Spawn Player ?!");
        Debug.Log(spawnPoint.transform.position);

        new WaitForSecondsRealtime(2);
            GameObject go = Instantiate(playerPrefab, spawnPoint.transform, true);
            NetworkServer.SpawnWithClientAuthority(go, connectionToClient);
        
    }
    /**
    IEnumerator spawnPlayer()
    {
        yield return new WaitForSeconds(4.0f);
        GameObject go = (GameObject)Instantiate(playerPrefab, spawnPoint.transform, true);
        NetworkServer.SpawnWithClientAuthority(go, connectionToClient);
    }
    **/
}
