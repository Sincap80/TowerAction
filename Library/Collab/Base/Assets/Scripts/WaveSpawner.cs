using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class WaveSpawner : NetworkBehaviour {

    [SyncVar]
    public int EnemiesAlive = 0;
    [SyncVar]
    private float countdown = 2f;
    [SyncVar]
    private int waveIndex = 0;

    public float timeBetweenWaves = 5f;
    public Text waveCountdownText;

    public Transform spawnPoint;
    public Transform[] spawnPoints;
    [SyncVar]
    public int spawnPointCounter = 0;

    public GameObject[] enemies;
    Wave wave;


    [Command]
    void CmdSetWaves()
    {
        wave = new Wave()
        {
            rate = 5,
            count = Random.Range(4, 18)
        };
        wave.enemy = new GameObject[wave.count];

        for (int i = 0; i < wave.count; i++)
            wave.enemy[i] = enemies[Random.Range(0, 3)];
    }
	void Update ()
	{
        if (EnemiesAlive > 0)
            return;

		if (countdown <= 0f)
		{
            GameObject.FindGameObjectWithTag("GameMaster").GetComponent<Waypoints>().setWaypoints(spawnPointCounter);
            StartCoroutine(SpawnWave());
			countdown = timeBetweenWaves;
            return;
		}

		countdown -= Time.deltaTime;
        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);

        waveCountdownText.text = string.Format("{0:00.00}", countdown);
	}

	IEnumerator SpawnWave ()
	{        
        GetComponent<PlayerStats>().Rounds++;
        CmdSetWaves();

        for (int i = 0; i < wave.count; i++)
		{
			CmdSpawnEnemy(wave.enemy[i]);
			yield return new WaitForSeconds(1f / wave.rate);
		}
        
        spawnPointCounter++;

        if (spawnPointCounter == 4)
            spawnPointCounter = 0;
    }

    [Command]
	void CmdSpawnEnemy (GameObject enemy)
	{
        
		Transform enemyT = Instantiate(enemy.transform, spawnPoints[spawnPointCounter].position, spawnPoints[spawnPointCounter].rotation);
        NetworkServer.Spawn(enemyT.gameObject);

        EnemiesAlive++;
        
	}

}
