﻿//
// Mecanimのアニメーションデータが、原点で移動しない場合の Rigidbody付きコントローラ
// サンプル
// 2014/03/13 N.Kobyasahi
//
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

// 必要なコンポーネントの列記
[RequireComponent(typeof (Animator))]
[RequireComponent(typeof (CapsuleCollider))]
[RequireComponent(typeof (Rigidbody))]

public class PlayerController : MonoBehaviour
{

	public float animSpeed = 1.5f;			
	public float lookSmoother = 3.0f;			
	public bool useCurves = true;				
												
	public float useCurvesHeight = 0.5f;	
    
	public float forwardSpeed = 7.0f;
	
	public float backwardSpeed = 2.0f;
	
	public float rotateSpeed = 2.0f;
	
	public float jumpPower = 3.0f; 
	
	private CapsuleCollider col;
	private Rigidbody rb;
	
	private Vector3 velocity;
	
	private float orgColHight;
	private Vector3 orgVectColCenter;
	
	private Animator anim;	
	private AnimatorStateInfo currentBaseState;			

	//private GameObject cameraObject;
		

	static int idleState = Animator.StringToHash("Base Layer.Idle");
	static int locoState = Animator.StringToHash("Base Layer.Locomotion");
	static int jumpState = Animator.StringToHash("Base Layer.Jump");
	static int restState = Animator.StringToHash("Base Layer.Rest");







    [SerializeField]
    private float speed = 5f;
    [SerializeField]
    private float lookSensitivity = 3f;

    [SerializeField]
    private float thrusterForce = 1000f;

    [SerializeField]
    private float thrusterFuelBurnSpeed = 1f;
    [SerializeField]
    private float thrusterFuelRegenSpeed = 0.3f;
    private float thrusterFuelAmount = 1f;

    public float GetThrusterFuelAmount()
    {
        return thrusterFuelAmount;
    }

    [SerializeField]
    private LayerMask environmentMask;

    [Header("Spring settings:")]
    [SerializeField]
    private float jointSpring = 20f;
    [SerializeField]
    private float jointMaxForce = 40f;

    // Component caching
    private PlayerMotor motor;
    private ConfigurableJoint joint;
    private Animator animator;












    void Start ()
	{


        motor = GetComponent<PlayerMotor>();
        joint = GetComponent<ConfigurableJoint>();
        animator = GetComponent<Animator>();

        SetJointSettings(jointSpring);


        anim = GetComponent<Animator>();
		
		col = GetComponent<CapsuleCollider>();
		rb = GetComponent<Rigidbody>();
		
		//cameraObject = GameObject.FindWithTag("MainCamera");
	
		orgColHight = col.height;
		orgVectColCenter = col.center;
}


    void Update()
    {
        /*if (PauseMenu.IsOn)
        {
            if (Cursor.lockState != CursorLockMode.None)
                Cursor.lockState = CursorLockMode.None;

            motor.Move(Vector3.zero);
            motor.Rotate(Vector3.zero);
            motor.RotateCamera(0f);

            return;
        }*/

        if (Cursor.lockState != CursorLockMode.Locked)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        //Setting target position for spring
        //This makes the physics act right when it comes to
        //applying gravity when flying over objects
        RaycastHit _hit;
        if (Physics.Raycast(transform.position, Vector3.down, out _hit, 100f, environmentMask))
        {
            joint.targetPosition = new Vector3(0f, -_hit.point.y, 0f);
        }
        else
        {
            joint.targetPosition = new Vector3(0f, 0f, 0f);
        }

        //Calculate movement velocity as a 3D vector
        float _xMov = Input.GetAxis("Horizontal");
        float _zMov = Input.GetAxis("Vertical");

        Vector3 _movHorizontal = transform.right * _xMov;
        Vector3 _movVertical = transform.forward * _zMov;

        // Final movement vector
        Vector3 _velocity = (_movHorizontal + _movVertical) * speed;

        // Animate movement
        animator.SetFloat("ForwardVelocity", _zMov);

        //Apply movement
        motor.Move(_velocity);

        //Calculate rotation as a 3D vector (turning around)
        float _yRot = Input.GetAxisRaw("Mouse X");

        Vector3 _rotation = new Vector3(0f, _yRot, 0f) * lookSensitivity;

        //Apply rotation
        motor.Rotate(_rotation);

        //Calculate camera rotation as a 3D vector (turning around)
        float _xRot = Input.GetAxisRaw("Mouse Y");

        float _cameraRotationX = _xRot * lookSensitivity;

        //Apply camera rotation
        motor.RotateCamera(_cameraRotationX);

        // Calculate the thrusterforce based on player input
        Vector3 _thrusterForce = Vector3.zero;
        if (Input.GetButton("Jump") && thrusterFuelAmount > 0f)
        {
            thrusterFuelAmount -= thrusterFuelBurnSpeed * Time.deltaTime;

            if (thrusterFuelAmount >= 0.01f)
            {
                _thrusterForce = Vector3.up * thrusterForce;
                SetJointSettings(0f);
            }
        }
        else
        {
            thrusterFuelAmount += thrusterFuelRegenSpeed * Time.deltaTime;
            SetJointSettings(jointSpring);
        }

        thrusterFuelAmount = Mathf.Clamp(thrusterFuelAmount, 0f, 1f);

        // Apply the thruster force
        motor.ApplyThruster(_thrusterForce);

    }

    private void SetJointSettings(float _jointSpring)
    {
        joint.yDrive = new JointDrive
        {
            positionSpring = _jointSpring,
            maximumForce = jointMaxForce
        };
    }




    /**



    float h = Input.GetAxis("Horizontal");			
    float v = Input.GetAxis("Vertical");			
    anim.SetFloat("Speed", v);							
    anim.SetFloat("Direction", h); 					
    anim.speed = animSpeed;							
    currentBaseState = anim.GetCurrentAnimatorStateInfo(0);
    rb.useGravity = true;
    


    velocity = new Vector3(0, 0, v);
    velocity = transform.TransformDirection(velocity);
    if (v > 0.1) {
        velocity *= forwardSpeed;	
    } else if (v < -0.1) {
        velocity *= backwardSpeed;	
    }
    
    if (Input.GetButtonDown("Jump")) {	
        
        if (currentBaseState.nameHash == locoState){
            if(!anim.IsInTransition(0))
            {
                    rb.AddForce(Vector3.up * jumpPower, ForceMode.VelocityChange);
                    anim.SetBool("Jump", true);	
            }
        }
    }
    
    
    transform.localPosition += velocity * Time.fixedDeltaTime;
    
    transform.Rotate(0, h * rotateSpeed, 0);	

    if (currentBaseState.nameHash == locoState){
        if(useCurves){
            resetCollider();
        }
    }
    else if(currentBaseState.nameHash == jumpState)
    {
        // refactor
        //cameraObject.SendMessage("setCameraPositionJumpView");
        if(!anim.IsInTransition(0))
        {
            if(useCurves){
                float jumpHeight = anim.GetFloat("JumpHeight");
                float gravityControl = anim.GetFloat("GravityControl"); 
                if(gravityControl > 0)
                    rb.useGravity = false;
                                    
                Ray ray = new Ray(transform.position + Vector3.up, -Vector3.up);
                RaycastHit hitInfo = new RaycastHit();
                if (Physics.Raycast(ray, out hitInfo))
                {
                    if (hitInfo.distance > useCurvesHeight)
                    {
                        col.height = orgColHight - jumpHeight;		
                        float adjCenterY = orgVectColCenter.y + jumpHeight;
                        col.center = new Vector3(0, adjCenterY, 0);	
                    }
                    else{				
                        resetCollider();
                    }
                }
            }		
            anim.SetBool("Jump", false);
        }
    }
    else if (currentBaseState.nameHash == idleState)
    {
        if(useCurves){
            resetCollider();
        }
        if (Input.GetButtonDown("Jump")) {
            anim.SetBool("Rest", true);
        }
    }
    else if (currentBaseState.nameHash == restState)
    {
        if(!anim.IsInTransition(0))
        {
            anim.SetBool("Rest", false);
        }
    }
}


void resetCollider()
{
    col.height = orgColHight;
    col.center = orgVectColCenter;
}

**/
}
