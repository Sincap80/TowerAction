﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class PauseMenu : NetworkBehaviour {

    public GameObject ui;

    public void Toggle()
    {
        ui.SetActive(!ui.activeSelf);

        if (ui.activeSelf)
        {
            //0 for pause, 1 for continue normal, 2 for doubletime
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }

    public void FastForward()
    {
        if (Time.timeScale == 1f)
            Time.timeScale = 2f;
        else
            Time.timeScale = 1f;
    }

    public void Retry()
    {
        Toggle();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void Quit()
    {
        Debug.Log("Quit Button pushed");
        SceneManager.LoadScene(0);
    }
}
